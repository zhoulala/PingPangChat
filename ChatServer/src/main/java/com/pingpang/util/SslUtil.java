package com.pingpang.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;

public class SslUtil {

    private static volatile SSLContext sslContext = null;

    public static SSLContext createSSLContext(String type ,InputStream ksInputStream ,String password) throws Exception {
        if(null == sslContext){
            synchronized (SslUtil.class) {
                if(null == sslContext){
                    // 支持JKS、PKCS12
                    KeyStore ks = KeyStore.getInstance(type);
                    // 证书存放地址
//                    InputStream ksInputStream = new FileInputStream(path);
                    //InputStream ksInputStream = SslUtil.class.getClass().getClassLoader().getResourceAsStream("keystore.p12");
                    ks.load(ksInputStream, password.toCharArray());
                    KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                    kmf.init(ks, password.toCharArray());
                    sslContext = SSLContext.getInstance("TLS");
                    sslContext.init(kmf.getKeyManagers(), null, null);
                }
            }
        }
        return sslContext;
    }
}
