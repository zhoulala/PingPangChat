package com.pingpang.util;

import java.util.HashMap;
import java.util.Map;

import com.pingpang.entity.Page;

public class PageUtil {

	/**
	 * 反回page 当前页码  默认1
	 * 返回limit 分页条数 默认10 
	 * 返回start 开始行号
	 * 返回 end   结束行号
	 * @param page  当前页
	 * @param limit 页大小
	 * @return
	 */
	public static Map<String,String> getPage(String page,String limit){
		Map<String,String> result=new HashMap<String,String>();
		if(StringUtil.isNUll(page) || !StringUtil.checkNum(page) ||Integer.valueOf(page)<=0) {
			page="1";
		}
		
		if(StringUtil.isNUll(limit) || !StringUtil.checkNum(limit) ||Integer.valueOf(limit)<=0) {
			limit="10";
		}
		
		int start=(Integer.valueOf(page)-1)*Integer.valueOf(limit);
		int end=start+Integer.valueOf(limit);
		
		result.put("page", String.valueOf(page));
		result.put("limit", String.valueOf(limit));
		result.put("start",String.valueOf(start));
		result.put("end", String.valueOf(end));
		return result;
	}
	
	public static Page getPage(Page page) {
		if(null==page) {
			page=new Page();
		}
		
		if(StringUtil.isNUll(page.getPage()) || !StringUtil.checkNum(page.getPage()) ||Integer.valueOf(page.getPage())<=0) {
			page.setPage("1");
		}
		
		if(StringUtil.isNUll(page.getLimit()) || !StringUtil.checkNum(page.getLimit()) ||Integer.valueOf(page.getLimit())<=0) {
			page.setLimit("10");
		}
		
		int start=(Integer.valueOf(page.getPage())-1)*Integer.valueOf(page.getLimit());
		int end=start+Integer.valueOf(page.getLimit());
		
		page.setStart(String.valueOf(start));
		page.setEnd(String.valueOf(end));
		
		return page;
	}
}
