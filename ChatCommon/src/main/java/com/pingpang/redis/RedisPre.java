package com.pingpang.redis;

public class RedisPre {

	/**
	 * redis中用户信息前缀 
	 */
	public final static String DB_USER="DB_USER_LOAD_";
	
	/**
	 * 用户再登录的时候，绑定服务端校验
	 */
	public final static String DB_USER_LOGIN_TOKEN="DB_USER_LOGIN_TOKEN_";
	
	/**
	 * redis群组信息前缀
	 */
	public final static String DB_GROUP="DB_GROUP_";
	
	/**
	 * redis群组成员前置
	 */
	public final static String DB_GROUP_SET="DB_GROUP_SET_";
	
	/**
	  * 服务端绑定上线用户
	 */
	public final static String NETTY_USER_SET="NETTY_USER_SET_";
	
	/**
	 * 发送消息得订阅
	 */
	public final static String SEND_MSG="SEND_MSG_";
	
	/**
	 * redis服务端负载均衡处理
	 */
	public final static String SERVER_ADDRES_ZSET="SERVER_ADDRES_ZSET_";
	
	/**
	 * 直播间
	 */
	public final static String AUDIO_LIVE_SET="AUDIO_LIVE_SET_";
	
	/**
	 * 第三方直播token
	 */
	public final static String AUDIO_LIVE_TOKEN="AUDIO_LIVE_TOKEN_"; 
}
