//---------------语音开始--------------------------    
//语音聊天请求
function onLineVoice() {
	$.confirm("是否发起语音通话?", "", function() {
		//成功操作
		sendMsg($("#userChatCode").html(), 14, "request");

		$.showLoading();
		setTimeout(function() {
			$.hideLoading();
		}, 3000);
	}, function() {
		//取消操作
	});
}

var recorder;
var audio;
function star() {
	audio = document.querySelector('#audio_from');
	playLoad();
	$('#audio_from').attr('src', "");
	HZRecorder.get(function(rec) {
		recorder = rec;
		recorder.start();
	}, {
		sampleBits : 8,
		sampleRate : 16000
	});

	/*  const constraints = {
			 audio: {
				 deviceId: {
				  // deviceInfo.deviceId
				  exact: audioSelect.value
				  }
				}
		    }; */
	//navigator.mediaDevices.enumerateDevices().then(gotDevices).catch(handleError);
	/* navigator.mediaDevices
	.getUserMedia(constraints)
	.then(gotDevices)
	.catch(handleError); */
}
function stop() {
	//recorder.stop();
	recorder.end();
	i = 0;
	audioIndex = 0;
	voiceArray = [];
	playVoicee = false;
}

function paly() {
	recorder.play(audio);
}

//绑定摄像头列表到下拉框
//var audioSelect;
function gotDevices(deviceInfos) {
	console.log("设备查找:" + deviceInfos);
	if (deviceInfos === undefined) {
		return
		

	}
	audioSelect = document.querySelector('select#audioSource');
	$("#audioSource").empty();
	console.log("所有设备:" + deviceInfos.length);
	for (let i = 0; i !== deviceInfos.length; ++i) {
		const deviceInfo = deviceInfos[i];
		const option = document.createElement('option');
		option.value = deviceInfo.deviceId;
		option.text = deviceInfo.kind + "||" + deviceInfo.label || 'device '
				+ (audioSelect.length + 1);
		audioSelect.appendChild(option);
	}
}
function handleError(error) {
	console.log('navigator.MediaDevices.getUserMedia error: ', error.message,
			error.name);
}

var blobChunk;
var CHUNK_SIZE = 1024 * 2;
var start = 0;
var end = CHUNK_SIZE;
function sendVoice(toUserCode) {
	if (null == toUserCode || "" == toUserCode) {
		toUserCode = $("#userChatCode").html();
	}

	if (null == toUserCode || "" == toUserCode) {
		console.log("接收方没找到用户名称");
		return;
	}
	recorder.start();
	var voiceBase64 = recorder.getPcm();
	recorder.clear();
	if (null == voiceBase64 || "" == voiceBase64) {
		return;
	}
	sendMsg(toUserCode, 14, voiceBase64);
}
var i = 0;
var audioIndex = 0;
var voiceArray = [];
var playVoicee = false;
var voiceQueue = new ArrayQueue();

function remotePlay(blobVoice) {
	console.log(i++);
	console.log("unZip1:" + blobVoice.length);
	//voiceArray.push(blobVoice);
	//const devices = navigator.mediaDevices.enumerateDevices();
	//const audioDevices = devices.filter(device => device.kind === 'audiooutput');
	//console.log("扬声器:"+audioDevices.length);
	//audio.setSinkId(audioDevices[0].deviceId);
	//console.log('Audio is being played on ' + audio.sinkId);
	if (!playVoicee) {
		var buffer = stringToUint8Array(blobVoice);
		var fileResult = addWavHeader(buffer, '16000', '8', '1');//解析数据转码wav
		audio.src = (window.URL || webkitURL).createObjectURL(fileResult);
		audio.volume = 1;//outside the range [0, 1]
		audio.loop = false;
		audio.play();
		audioIndex++;
		playVoicee = true;
	} else {
		voiceQueue.push(blobVoice);
	}
	/* audioContext.decodeAudioData(fileResult, function(buffer) {
		   _visualize(audioContext,buffer);//播放
		}); */
}

function playLoad() {
	audio.addEventListener('ended', function() {
		try {
			//console.log("audioIndex:"+audioIndex);
			console.log("voiceQueue:" + voiceQueue.size());
			if (voiceQueue.size() > 0) {
				//var buffer=stringToUint8Array(voiceArray[audioIndex]);
				var buffer = stringToUint8Array(voiceQueue.pop());
				var fileResult = addWavHeader(buffer, '16000', '8', '1');//解析数据转码wav
				audio.src = (window.URL || webkitURL)
						.createObjectURL(fileResult);
				;
				audio.volume = 1;//outside the range [0, 1]
				audio.loop = false;
				audio.play();
				audioIndex++;
			} else {
				playVoicee = false;
			}
		} catch (error) {
			console.error(error);
		}
	}, false);
}

function stringToUint8Array(str) {
	var arr = [];
	for (var i = 0, j = str.length; i < j; ++i) {
		arr.push(str.charCodeAt(i));
	}

	var tmpUint8Array = new Uint8Array(arr);
	return tmpUint8Array
}
//---------------语音结束--------------------------