<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/weui/css/weui.css"/>
    <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/weui/css/weuix.css"/>
    <script src="${httpServletRequest.getContextPath()}/weui/js/zepto.min.js"></script>
    <script src="${httpServletRequest.getContextPath()}/weui/js/zepto.weui.js"></script>
</head>
<body>
 <div class="page v-center">
    <form class="weui-form" name="form" id="form" action="" method="POST">
        <div class="weui-form__control-area">
            <div class="weui-cells__group weui-cells__group_form">
                <div class="weui-cells__title"></div>
                <div class="weui-cells weui-cells_form">
				   
				   <div class="weui-cell weui-cell_active" id="js_userName_cell">
                        <div class="weui-cell__hd"><label class="weui-label">昵称</label></div>
                        <div class="weui-cell__bd">
                            <input id="js_userName_input" name="userName" class="weui-input" autofocus type="weui-input" placeholder="请输入用户名" maxlength="32" />
						</div>
                    </div>
					
                    <div class="weui-cell weui-cell_active" id="js_userCode_cell">
                        <div class="weui-cell__hd"><label class="weui-label">账号</label></div>
                        <div class="weui-cell__bd">
                            <input id="js_userCode_input" name="userCode" class="weui-input" autofocus type="weui-input" placeholder="请输入账号代码" pattern="[0-9A-Za-z]*" maxlength="32" />
						</div>
                    </div>
					
                    <div class="weui-cell weui-cell_active" id="js_userPassword_cell">
                        <div class="weui-cell__hd "><label class="weui-label">密码</label></div>
                        <div class="weui-cell__bd">
                            <input id="js_userPassword_input" name="userPassword" class="weui-input" type="weui-input" placeholder="请输入密码" pattern="[0-9A-Za-z]*" maxlength="32"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="weui-form__opr-area">
            <a class="weui-btn weui-btn_primary" href="javascript:" id="login">登录</a>
			<a class="weui-btn weui-btn_primary" href="javascript:" id="regist">注册</a>
        </div>
    </form>
</div>
<script type="text/javascript">
$(function(){
	            //错误消息提醒
				if (null != "${userCode}" && "" != "${userCode}") {
					 $("#js_userCode_input").val("${userCode}");
				}
				
				if (null != "${userCode}" && "" != "${userCode}") {
					 $("#js_userCode_input").val("${userCode}");
				}
	            
				if (null != "${userPassword}" && "" != "${userPassword}") {
					 $("#js_userPassword_input").val("${userPassword}");
				}
				
				var errorMsg = "${errorMsg}";
				if (null != errorMsg && "" != errorMsg) {
					$.toptip(errorMsg);
				}
				
               $("#js_userName_cell").hide();
               
               //登录校验
               $(document).on("click","#login",function(){
			       //$("#js_userName_cell").removeClass('show').addClass('hide');
                   if($("#js_userName_cell").is(':visible')){
				     $("#js_userName_cell").hide();
					 return false;
				   }
				   var userCode = $("#js_userCode_input").val();
				   var userPassword = $("#js_userPassword_input").val();
                   if(userCode==""){
                       $.toptip("账号不能为空");
                       return false;
                   }
				   if(userPassword==""){
                       $.toptip("密码不能为空");
                       return false;
                   }
				   document.form.action="${httpServletRequest.getContextPath()}/user/chat";
				   document.getElementById("form").submit();
				   //window.location.href="${httpServletRequest.getContextPath()}/user/chat?userCode="+userCode+"&userPassword="+userPassword;
               })
			   
               //注册校验
			   $(document).on("click","#regist",function(){
			       //$("#js_userName_cell").removeClass('hide').addClass('show');
				   if($("#js_userName_cell").is(':hidden')){
				      $("#js_userName_cell").show();
					  return false;
				   }
				   
                   var userName = $("#js_userName_input").val();
				   var userCode = $("#js_userCode_input").val();
				   var userPassword = $("#js_userPassword_input").val();
				   if(userCode==""){
                       $.toptip("账号不能为空");
                       return false;
                   }
                   if(userName==""){
                       $.toptip("昵称不能为空");
                       return false;
                   }
				   if(userPassword==""){
                       $.toptip("密码不能为空");
                       return false;
                   }
				   document.form.action="${httpServletRequest.getContextPath()}/user/addUser";
				   document.getElementById("form").submit();
               })
});
</script>
</body>
</html>